﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Noto
{
    [XmlType("Paragraph")]
   public class ParagraphBlock : IRenderableBlock
    {
        [XmlAttribute("Text")]
        public string _content = "";
        [XmlIgnore]
        public bool isActive = false;
        [XmlIgnore]
        public int cursorPos = 0;
        [XmlIgnore]
        public Font font = new Font("Tahoma", 10, FontStyle.Regular);
        [XmlAttribute("FontSize")]
        public float FontSize
        {
            get
            {
                return font.Size;
            }
            set
            {
                if (font == null) font = new Font("Tahoma", value, FontStyle.Regular);
                else font = new Font(font.Name, value, font.Style);
            }
        }
        [XmlAttribute("FontStyle")]
        public FontStyle FontType
        {
            get
            {
                return font.Style;
            }
            set
            {
                if (font == null) font = new Font("Tahoma", 10, value);
                else font = new Font(font.Name, font.Size, value);
            }
        }
        [XmlIgnore]
        private Brush blackBrush = new SolidBrush(Color.Black);
        [XmlIgnore]
        private static StringFormat ef = new StringFormat();
        [XmlIgnore]
        private static Pen dm = new Pen(Color.DarkMagenta, 0.7f);
        [XmlIgnore]
        private static Pen bp = new Pen(Color.Black, 0.5f);
        [XmlIgnore]
        private float cachedHeight = 0.0f;
        public void RenderOnGraphicsAt(Graphics g, RectangleF at, RenderView parent)
        {
            var rh = this.RenderingHeight(g, parent);
            if (isActive)
                g.DrawLine(dm, Convert.ToInt32(at.X - 0.7), Convert.ToInt32(at.Y), Convert.ToInt32(at.X - 0.7), Convert.ToInt32(at.Y + rh));
            g.DrawString(_content, font, blackBrush, new RectangleF(at.X, at.Y, parent.Width - at.X * 2, rh), ef);
            if (isActive)
            {
                SizeF f = g.MeasureString((_content.Length == 0 ? " " : _content.Substring(0, Math.Min(cursorPos, _content.Length))), font);

                g.DrawLine(bp, (_content.Length == 0 ? 0 : Convert.ToInt32(at.X + f.Width + 1)), (int)at.Y, (_content.Length == 0 ? 0 : Convert.ToInt32(at.X + f.Width + 1)), Convert.ToInt32(at.Y + f.Height));
            } 

        }
    
        public void Activate()
        {
            isActive = true;
        }
        public void Deactivate()
        {
            isActive = false;
        }
        public bool IsActive()
        {
            return isActive;
        }
        public float RenderingHeight(Graphics g, RenderView parent)
        {
            if (cachedHeight == 0) UpdateHeight(g, parent);
            return cachedHeight;
        }
        private void UpdateHeight(Graphics g, RenderView parent)
        {
            SizeF f = g.MeasureString(_content + " |", font);
            cachedHeight= f.Height * (float)Math.Ceiling(f.Width / (parent.Width - parent.sideMargin * 4)); // TODO!
        }
        [XmlIgnore]
        bool suppressNextInput = false;
        [XmlIgnore]
        bool insertMode = true;
        public void HandleTextInputEvent(string keys, RenderView parent)
        {
            if (suppressNextInput)
            {
                suppressNextInput = false;
                return;
            }
            if (keys[0] == '\t' && cursorPos == _content.Length)
            {
                NotoDocument doc = parent.currentDocument;
                int i = 0;
                bool found = false;
                string _cl = _content.ToLower();
                for (i = 0; i < doc.shortcutKeys.Count; i++)
                {
                    if (_cl.EndsWith(doc.shortcutKeys[i].ToLower()))
                    {
                        found = true; break;
                    }
                }
                if (found)
                {
                    string sk = doc.shortcutKeys[i];
                    string sv = doc.shortcutValues[i];
                    parent.SetStateText(sk + " -> " + sv);
                    _content = _content.Substring(0, _content.Length - sk.Length) + sv;
                    cursorPos = _content.Length;
                }
                else
                {
                    parent.SetStateText("Shortcut not found :(");
                    _content += "    ";
                    cursorPos = _content.Length;
                }
                return;
            }
            if (insertMode)
            {
                parent.lblState.Text = "Edit: insert";
                if (cursorPos == _content.Length)
                {
                    _content += keys;
                }
                else
                {
                    _content = _content.Substring(0, cursorPos + 1) + keys + _content.Substring(cursorPos + 1, _content.Length - (cursorPos + 1));
                }
            }
            else
            {
                // TODO??? OVR mode
            }
            cursorPos += keys.Length;
            UpdateHeight(parent.graphics,parent);
        }
        public void HandleKeyInputEvent(byte key, bool ctl, bool alt, bool shift, RenderView parent)
        {
            UpdateHeight(parent.graphics,parent);
            if (key == 13)
            {
                if (ctl)
                {
                    // what happens is: _content += "\n";
                }
                else
                {
                    // enter'
                    IRenderableBlock newone = new ParagraphBlock();
                    newone.Activate();
                    this.Deactivate();
                    parent.currentDocument.blocks.Add(newone);
                    parent.Redraw();
                }
            }
            else if (key == 8 && _content.Length > 0 && cursorPos > 0)
            {

                if (!ctl)
                {
                    // just a bksp
                    if (cursorPos == _content.Length)
                    {
                        _content = _content.Substring(0, _content.Length - 1);
                    }
                    else
                    {
                        _content = _content.Substring(0, cursorPos - 1) + _content.Substring(cursorPos, _content.Length - cursorPos);
                    }
                    cursorPos -= 1;
                }
                else
                {
                    if (_content.Length > 0)
                    {
                        if (cursorPos < _content.Length && _content[cursorPos] != ' ') return;
                        //bksp a whole word
                        suppressNextInput = true;
                        string[] WordsBeforeCursor = _content.Substring(0, cursorPos - 1).Split(' ');
                        string WordBeforeCursor = WordsBeforeCursor[WordsBeforeCursor.Length - 1];
                        _content = _content.Substring(0, cursorPos - WordBeforeCursor.Length - 1) + _content.Substring(cursorPos, _content.Length - cursorPos);
                        cursorPos -= WordBeforeCursor.Length + 1;
                    }
                }
            }
            else if (key == (byte)Keys.Left)
            {
                if (cursorPos == 0)
                {
                    parent.GoToPrevBlock();
                }
                else
                {
                    if (ctl)
                    {
                        // skip a word
                        if (cursorPos >= _content.Length) cursorPos = _content.Length - 1;
                        while (cursorPos >0 && _content[cursorPos] != ' ') cursorPos--;
                        while (cursorPos >0 && _content[cursorPos] == ' ') cursorPos--;
                        
                    }
                    else
                    {
                        cursorPos = Math.Max(0, cursorPos - 1);
                    }
                }
            }
            else if (key == (byte)Keys.Delete)
            {
                if (cursorPos == _content.Length) return;
                if (ctl)
                {
                    // erase whole word
                    if (_content[cursorPos] != ' ') return;
                    string[] WordsAfterCursor = _content.Substring(cursorPos-1, _content.Length - cursorPos + 1).Split(' ');
                    string WordAfterCursor = WordsAfterCursor[WordsAfterCursor.Length - 1];
                    _content = _content.Substring(0, cursorPos) + _content.Substring(cursorPos + WordAfterCursor.Length, _content.Length - cursorPos - WordAfterCursor.Length);
                    //cursorPos -= WordBeforeCursor.Length + 1;
                }
                else
                {
                    // erase one char
                    _content = _content.Substring(0, cursorPos) + _content.Substring(cursorPos+1, _content.Length - cursorPos-1);
                }
            }
            else if (key == (byte)Keys.Right)
            {
                if (cursorPos == _content.Length)
                {
                    parent.GoToNextBlock();
                }
                else
                {
                    if (ctl)
                    {
                        // skip a word
                        while (cursorPos < _content.Length && _content[cursorPos] != ' ') cursorPos++;
                        while (cursorPos < _content.Length && _content[cursorPos] == ' ') cursorPos++;
                    }
                    else
                    {
                        // skip a char
                        cursorPos = Math.Min(cursorPos + 1, _content.Length);
                    }
                }
            }
            else if (key == (byte)Keys.B && ctl)
            {
                suppressNextInput = true;
                parent.lblState.Text = "Bold font";
                font = new Font(font.Name, font.Size, font.Style ^ FontStyle.Bold);
            }
            else if (key == (byte)Keys.I && ctl)
            {
                suppressNextInput = true;
                parent.lblState.Text = "Italic font";
                font = new Font(font.Name, font.Size, font.Style ^ FontStyle.Italic);
            }
            else if (key == (byte)Keys.U && ctl)
            {
                suppressNextInput = true;
                parent.lblState.Text = "Underline font";
                font = new Font(font.Name, font.Size, font.Style ^ FontStyle.Underline);
            }
            else if (key == (byte)Keys.S && ctl && shift)
            {
                suppressNextInput = true;
                parent.lblState.Text = "Strike font";
                font = new Font(font.Name, font.Size, font.Style ^ FontStyle.Strikeout);
            }
            else if (key == (byte)Keys.D1 && ctl)
            {
                parent.lblState.Text = "Add Heading (Level 1)";
                IRenderableBlock newone = new HeadingBlock(HeadingSizes.FirstLevel);
                newone.Activate();
                this.Deactivate();
                parent.currentDocument.blocks.Add(newone);
                parent.Redraw();
            }
            else if (key == (byte)Keys.D2 && ctl)
            {
                parent.lblState.Text = "Add Heading (Level 2)";
                IRenderableBlock newone = new HeadingBlock(HeadingSizes.SecondLevel);
                newone.Activate();
                this.Deactivate();
                parent.currentDocument.blocks.Add(newone);
                parent.Redraw();
            }
            else if (key == (byte)Keys.D3 && ctl)
            {
                parent.lblState.Text = "Add Heading (Level 3)";
                IRenderableBlock newone = new HeadingBlock(HeadingSizes.ThirdLevel);
                newone.Activate();
                this.Deactivate();
                parent.currentDocument.blocks.Add(newone);
                parent.Redraw();
            }
            else if (key == (byte)Keys.M && ctl)
            {
                MathBlock b = new MathBlock();
                b.content = "";
                parent.DeactivateAllBlocks();
                b.Activate();
                parent.currentDocument.blocks.Add(b);
                parent.Redraw();
                MathInput mi = new MathInput(b);
                mi.Show();
            }
            else
            {
                // Console.WriteLine("HEADING: Unknown key event " + key.ToString());
            }


            return;
        }

        // Touch events todo
        public void TouchBegan(Point at, RenderView parent) { }
        public void TouchMoved(Point at, RenderView parent) { }
        public void TouchEnded(Point at, RenderView parent) { }

        public string TreeDisplay()
        {
            return (_content.Length == 0 ? "<empty paragraph>":Utils.Ellipsis(_content, 50));
        }
        public bool IsEmpty()
        {
            return _content.Length == 0;
        }
    }
}
