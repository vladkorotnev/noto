﻿namespace Noto
{
    partial class MathInput
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbRender = new System.Windows.Forms.PictureBox();
            this.tbFormula = new System.Windows.Forms.TextBox();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.hintView = new System.Windows.Forms.MenuItem();
            this.refreshTimer = new System.Windows.Forms.Timer();
            this.SuspendLayout();
            // 
            // pbRender
            // 
            this.pbRender.Location = new System.Drawing.Point(4, 4);
            this.pbRender.Name = "pbRender";
            this.pbRender.Size = new System.Drawing.Size(233, 78);
            this.pbRender.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // tbFormula
            // 
            this.tbFormula.Location = new System.Drawing.Point(4, 89);
            this.tbFormula.Multiline = true;
            this.tbFormula.Name = "tbFormula";
            this.tbFormula.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbFormula.Size = new System.Drawing.Size(233, 176);
            this.tbFormula.TabIndex = 1;
            this.tbFormula.TextChanged += new System.EventHandler(this.tbFormula_TextChanged);
            this.tbFormula.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbFormula_KeyDown);
            this.tbFormula.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFormula_KeyPress);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.hintView);
            // 
            // hintView
            // 
            this.hintView.Enabled = false;
            this.hintView.Text = "Hints will appear here";
            // 
            // refreshTimer
            // 
            this.refreshTimer.Interval = 500;
            this.refreshTimer.Tick += new System.EventHandler(this.refreshTimer_Tick);
            // 
            // MathInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tbFormula);
            this.Controls.Add(this.pbRender);
            this.Menu = this.mainMenu1;
            this.Name = "MathInput";
            this.Text = "Input LaTeX";
            this.Activated += new System.EventHandler(this.MathInput_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MathInput_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbRender;
        private System.Windows.Forms.TextBox tbFormula;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem hintView;
        private System.Windows.Forms.Timer refreshTimer;
    }
}