﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Noto
{
    public partial class MathInput : Form
    {
        private MathBlock editedBlock;
        public MathInput(MathBlock b)
        {
            InitializeComponent();
            editedBlock = b;
            tbFormula.Text = editedBlock.content;
            pbRender.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        static private Dictionary<string, string> hints = new Dictionary<string, string>{
            {"\\frac","frac {a} {b}: fraction"},
            {"\\int","int [_lower] [^higher] {f}: integral"},
            {"\\v","v {a}: vector"}, {"vec","vec {a}: vector"},
            {"\\line","line(width,height): draw a line"}, {"\\circle","circle(width,height): draw a circle"},
            {"\\mathbb","mathbb: style like R as Real numbers"},
            {"\\mathca","mathca: style of cursive (caps only)"},
            {"\\mathscr","mathscr: style of script (caps only)"},
            {"\\text","text: render as text"},
            {"\\cdot","cdot: dot"}, {"\\div","div: division sign"}, 
            {"\\mp","mp: minus-plus"}, {"\\otimes","otimes: xor sign"},
            {"\\bigcirc","bigcirc: big circle"},{"\\asymp","asymp: asymptote"},
            {"\\approx","approx: approximate sign"}, {"\\ll","ll: signif. less than sign"},
            {"\\gg","gg: signif. more than sign"},{"\\uparrow","up arrow"},{"\\downarrow","down arrow"},{"\\leftarrow","left arrow"},{"\\rightarrow","right arrow"},{"\\nearrow","NE arrow"},{"\\searrow","SE arrow"},{"\\nwarrow","NW arrow"},
            {"\\times","times: multiplication sign"},
            {"\\leq","leq: less or equal"},{"\\geq","greater or equal"},
            {"\\subset","subset of"},{"\\supset","superset of"},{"\\pm","pm: plus-minus"},
            {"\\leftrightarrow","left-right arrow"},{"\\sim","similar"},{"\\forall","for each"},
            {"\\sum","sum"}
        };

        private void tbFormula_TextChanged(object sender, EventArgs e)
        {
            
            
            foreach(string key in hints.Keys) {
                if (tbFormula.Text.Substring(0,tbFormula.SelectionStart).EndsWith(key))
                {
                    hintView.Text =hints[key];
                    break;
                }
            }
            refreshTimer.Enabled = false; refreshTimer.Enabled = true;
            
        }

     

        private void MathInput_Activated(object sender, EventArgs e)
        {
            tbFormula.SelectionStart = tbFormula.Text.Length;
            
        }
        private void updateFormula()
        {
            editedBlock.content = tbFormula.Text.Replace("`","\\").Replace("\\_","_"); // fix LEng bug

            try
            {
                pbRender.Image = editedBlock.cachedBitmap;
                if (pbRender.Image.Width > pbRender.Width) pbRender.SizeMode = PictureBoxSizeMode.StretchImage;
                else pbRender.SizeMode = PictureBoxSizeMode.CenterImage;
            }
            catch (Exception exc)
            {
                // hintView.Text = "Formula possibly invalid or MimeTeX error";

            }
        }
        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            updateFormula();
            refreshTimer.Enabled = false;
        }

        private void MathInput_Closing(object sender, CancelEventArgs e)
        {
            updateFormula();
        }

        private bool suppressNext = false;
        private void tbFormula_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void tbFormula_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }
    }
}