﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Drawing;
using System.Diagnostics;



namespace Noto
{
    public static class MimeTexRenderer
    {
        private static string usagePath;
        public static bool canRenderMath = false;
        public static readonly bool antialias = false;
        public static void UpdateUsagePath() {
            usagePath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\Resources\\mimetex_";
            if(antialias && File.Exists(usagePath+"aa.exe")) {
                usagePath += "aa.exe";
            } else {
                usagePath += "gif.exe";
            }
            if (!File.Exists(usagePath))
            {
                MessageBox.Show("MimeTeX is not found at path: \r\n" + usagePath + "\r\nMath input and display will not work", "Math Input Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                canRenderMath = false;
            }
            else
            {
                canRenderMath = true;
            }
        }
        public static Bitmap RenderFormula(string formula)
        {
            Process p = new Process();
            // Redirect the output stream of the child process.
            string fn = "mathtmp_"+System.Guid.NewGuid().ToString().Replace("-","")+".gif";
            p.StartInfo = new ProcessStartInfo(usagePath, "-e "+fn+" \"" + formula.Replace("\r\n", "") + "\"");
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.WorkingDirectory = "\\";
            p.Start();
            p.WaitForExit();
           
            Bitmap result = new Bitmap("\\"+fn);
            p.Dispose();
            File.Delete("\\" + fn);
            return result;
        }
    }
    [XmlType("Math")]
    public class MathBlock : IRenderableBlock
    {
        [XmlIgnore]
        private string _content = "";
        [XmlAttribute("formula")]
        public string content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
                try
                {
                    cachedBitmap = MimeTexRenderer.RenderFormula(content);
                }
                catch (Exception e) { }
            }
        }
        [XmlIgnore]
        public Bitmap cachedBitmap;
        public void RenderOnGraphicsAt(Graphics g, RectangleF at, RenderView parent)
        {
            if (content.Length == 0) return;
            if (!MimeTexRenderer.canRenderMath)
            {
                g.DrawString("Math engine not ready...", new Font("Courier New", 14, FontStyle.Italic), new SolidBrush(Color.Red), at);
            }
            else
            {
                try
                {
                   if(cachedBitmap == null) 
                      cachedBitmap = MimeTexRenderer.RenderFormula(content);
                   float scaleFactor = Math.Min(at.Width / cachedBitmap.Width, at.Height / cachedBitmap.Height);
                    g.DrawImage(cachedBitmap, new Rectangle(Convert.ToInt32(at.X), Convert.ToInt32(at.Y), Convert.ToInt32(cachedBitmap.Width*scaleFactor), Convert.ToInt32( cachedBitmap.Height*scaleFactor)), new Rectangle(0, 0, cachedBitmap.Width, cachedBitmap.Height), GraphicsUnit.Pixel);
                    
                }
                catch (Exception e)
                {
                    g.DrawString("Math equation bad", new Font("Courier New", 14, FontStyle.Italic), new SolidBrush(Color.Red), at);
                }
            }
        }
        public float RenderingHeight(Graphics g, RenderView parent)
        {
            if (content.Length == 0 || cachedBitmap == null) return 20;
            return 80;
           
        }
        public void HandleKeyInputEvent(byte key, bool ctl, bool alt, bool shift, RenderView parent)
        {
            this.Deactivate();
            if (parent.currentDocument.blocks.IndexOf(this) == parent.currentDocument.blocks.Count - 1)
            {
                ParagraphBlock h = new ParagraphBlock();
                parent.currentDocument.blocks.Add(h);
                h.Activate();
            }
            else
            {
                IRenderableBlock b = parent.currentDocument.blocks[parent.currentDocument.blocks.IndexOf(this) + 1];
                b.Activate();
            }
        }
        public void HandleTextInputEvent(string keys, RenderView parent)
        {
            
        }

        [XmlIgnore]
        private bool isActive = false;
        public void Activate()
        {
            isActive = true;

            //parent.SetStateText("Math: "+content);
        }
        public void Deactivate()
        {
            isActive = false;
        }
        public bool IsActive()
        {
            return isActive;
        }

        public void TouchBegan(Point at, RenderView parent)
        {
            
           
        }
        public void TouchMoved(Point at, RenderView parent)
        {

        }
        public void TouchEnded(Point at, RenderView parent)
        {
            MathInput mi = new MathInput(this);
            mi.Show();
        }
        public string TreeDisplay()
        {
            return content;
        }
        public bool IsEmpty()
        {
            return (content.Length == 0);
        }

    }
}
