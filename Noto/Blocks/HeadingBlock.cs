﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Serialization;
public enum HeadingSizes
{
    FirstLevel = 16, SecondLevel = 14, ThirdLevel = 12

}
namespace Noto
{
    [XmlType("Heading")]
    public class HeadingBlock : ParagraphBlock
    {
        
        public HeadingBlock()
        {
            this.font = new Font("Tahoma", (float)HeadingSizes.FirstLevel, FontStyle.Bold);
        }
        public HeadingBlock(int fontsize)
        {
            this.font = new Font("Tahoma", fontsize, FontStyle.Bold);
        }
        public HeadingBlock(HeadingSizes fontsize)
        {
            this.font = new Font("Tahoma", (float)fontsize, FontStyle.Bold);
        }
    }
}
