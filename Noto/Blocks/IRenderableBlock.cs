﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Xml.Serialization;

namespace Noto
{
   public interface IRenderableBlock
    {

       void RenderOnGraphicsAt(Graphics g, RectangleF at, RenderView parent);
       float RenderingHeight(Graphics g, RenderView parent);
       void HandleKeyInputEvent(byte key, bool ctl, bool alt, bool shift, RenderView parent);
       void HandleTextInputEvent(string keys, RenderView parent);
       void Activate();
       void Deactivate();
       bool IsActive();
       void TouchBegan(Point at, RenderView parent);
       void TouchMoved(Point at, RenderView parent);
       void TouchEnded(Point at, RenderView parent);
       string TreeDisplay();
       bool IsEmpty();

    }
}
