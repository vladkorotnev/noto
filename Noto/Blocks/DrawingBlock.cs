﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Xml.Serialization;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

public enum DrawingImageEncodingType
{
    DrawingEncodingBase64, DrawingEncodingString
}
namespace Noto
{
    [XmlType("Drawing")]
    public class DrawingBlock : IRenderableBlock
    {
        [XmlIgnore]
        static Brush whiteBrush = new SolidBrush(Color.White);
        public DrawingBlock()
        {

            drawing = new Bitmap(1, 150);
            dG = Graphics.FromImage(drawing);
            dG.Clear(Color.White);
       
        }
        public DrawingBlock(Bitmap preDrawing)
        {
            drawing = preDrawing;
            dG = Graphics.FromImage(drawing);
            
        }
        [XmlIgnore]
        private bool isActive = false;
        [XmlIgnore]
        public Bitmap drawing;
        [XmlIgnore]
        private float cachedHeight;
        [XmlIgnore]
        private Graphics dG;
        [XmlAttribute("ImageEncoding")]
        public DrawingImageEncodingType imageEncoding = DrawingImageEncodingType.DrawingEncodingBase64; //def
        [XmlAttribute("ImageData")]
        public string drawingXML
        {
            get
            { // serialize
                if (drawing == null)
                {
                    drawing = new Bitmap(1, 1);
                    dG = Graphics.FromImage(drawing);
                    dG.Clear(Color.White);
                }
                switch (imageEncoding)
                {
                    case DrawingImageEncodingType.DrawingEncodingBase64:
                        return Convert.ToBase64String(Utils.GetByteArray(drawing));
                        break;

                    case DrawingImageEncodingType.DrawingEncodingString:
                        return Utils.GetString(Utils.GetByteArray(drawing));
                        break;
                    default:
                        return "ERROR";
                        break;
                }
                
            }
            set
            { // deserialize
                if(value.Length>0){
                    byte[] b;
                    switch (imageEncoding)
                    {
                        case DrawingImageEncodingType.DrawingEncodingBase64:
                            b = Convert.FromBase64String(value);
                            break;
                        case DrawingImageEncodingType.DrawingEncodingString:
                            b = Utils.GetBytes(value);
                            break;
                        default:
                            b = new byte[0]{};
                            break;
                    }
                    firstTimeExtension = false;
                    using (MemoryStream ms = new MemoryStream(b))
                    {
                        drawing = new Bitmap(ms);
                        dG = Graphics.FromImage(drawing);
                    }
                }
            }
        }
        
        public void RenderOnGraphicsAt(Graphics g, RectangleF at, RenderView parent)
        {
            
            if(isActive)g.DrawRectangle(new Pen(Color.Gray,0.2f), Convert.ToInt32(at.X-1),Convert.ToInt32(at.Y-1),Convert.ToInt32(at.Width+1),Convert.ToInt32(at.Height+1));
            g.DrawImage(drawing, Convert.ToInt32(at.X), Convert.ToInt32(at.Y));
        }
        private void UpdateHeight(Graphics g, RenderView parent)
        {
            _createIfNeeded(parent);
            if (drawing.Width < parent.Width - 2 * parent.sideMargin) _extendRight(Convert.ToInt32((parent.Width - 2 * parent.sideMargin) - drawing.Width));
            cachedHeight = drawing.Height;
        }
        public float RenderingHeight(Graphics g, RenderView parent)
        {
            if (cachedHeight == 0) UpdateHeight(g, parent);  
            return cachedHeight;
        }
        public void HandleKeyInputEvent(byte key, bool ctl, bool alt, bool shift, RenderView parent)
        {
            // no key evts
            if(ctl)isErasing = !isErasing;
            parent.SetStateText("Eraser: " + (isErasing ? "ON" : "Off"));
        }
        [XmlIgnore]
        static private Brush textBrush = new SolidBrush(Color.Black);
        [XmlIgnore]
        static private Font textFont = new Font("Courier New", 10.0f, FontStyle.Regular);
        public void HandleTextInputEvent(string keys, RenderView parent)
        {
            // no key evts
            if (isPenDown && last != null)
            {
                SizeF ts = dG.MeasureString(keys, textFont);
                dG.DrawString(keys, textFont, textBrush,(float) (last.X - ts.Width/2),(float) (last.Y-ts.Height/2));
            }
        }
        public void Activate()
        {
            isActive = true;
        }
        public void Deactivate()
        {
            isActive = false;
        }
        public bool IsActive()
        {
            return isActive;
        }
        private void _createIfNeeded(RenderView parent)
        {
            if (drawing == null)
            {
                drawing = new Bitmap(parent.Width-Convert.ToInt32(parent.sideMargin*2), Screen.PrimaryScreen.Bounds.Height);
                dG = Graphics.FromImage(drawing);
                dG.Clear(Color.White);
                UpdateHeight(parent.graphics, parent);
            }
        }
        
        private void _extendDown(int toAdd, RenderView parent)
        {
            Graphics orig = dG;
            Bitmap origI = (Bitmap)drawing;
            drawing = new Bitmap(origI.Width, origI.Height + toAdd);
            dG = Graphics.FromImage(drawing);
            dG.DrawImage(origI, 0, 0);
            dG.FillRectangle(whiteBrush, 0, origI.Height, drawing.Width, drawing.Height - origI.Height);
            orig.Dispose();
            origI.Dispose();
            UpdateHeight(parent.graphics, parent);
        }
        private void _extendRight(int toAdd)
        {
            Graphics orig = dG;
            Bitmap origI = (Bitmap)drawing;
            drawing = new Bitmap(origI.Width+toAdd, origI.Height);
            dG = Graphics.FromImage(drawing);
            dG.DrawImage(origI, 0, 0);
            dG.FillRectangle(whiteBrush, origI.Width, 0, drawing.Width - origI.Width, drawing.Height);
            orig.Dispose();
            origI.Dispose();
        }
        [XmlIgnore]
        static Pen drawPen = new Pen(Color.Black, 1.5f);
        [XmlIgnore]
        static Pen erasePen = new Pen(Color.White, 3.5f);
        [XmlIgnore]
        private bool isErasing = false;
        [XmlIgnore]
        private  Point last;
        [XmlIgnore]
        bool isPenDown = false;
        [XmlIgnore]
        bool firstTimeExtension = true;
        private void _extendBelowDueToBleedingEdge(RenderView parent)
        {
            if (firstTimeExtension)
            {
                _extendDown(Screen.PrimaryScreen.Bounds.Height, parent);
                firstTimeExtension = false;
            }
            else
            {
                _extendDown(20, parent);
            }
        }
        public void TouchBegan(Point at, RenderView parent)
        {
            _createIfNeeded(parent); UpdateHeight(parent.graphics, parent);
            last = at;
            isPenDown = true;
            if (drawing.Height - at.Y <= 15 && !isErasing) _extendBelowDueToBleedingEdge(parent);
            parent.SetStateText("Drawing input");
        }
        public void TouchMoved(Point at, RenderView parent)
        {
            _createIfNeeded(parent);
            if (cachedHeight - at.Y <= 15 && !isErasing) _extendBelowDueToBleedingEdge(parent);
            dG.DrawLine(isErasing?erasePen:drawPen,Convert.ToInt32(last.X),Convert.ToInt32(last.Y),Convert.ToInt32(at.X),Convert.ToInt32(at.Y));
            last = at;
        }
        public void TouchEnded(Point at, RenderView parent)
        {
            parent.SetStateText("Drawing finished");
            _createIfNeeded(parent); UpdateHeight(parent.graphics, parent);
            if (drawing.Height - at.Y <= 15 && !isErasing) _extendBelowDueToBleedingEdge(parent);
            dG.DrawLine(isErasing ? erasePen : drawPen, Convert.ToInt32(last.X), Convert.ToInt32(last.Y), Convert.ToInt32(at.X), Convert.ToInt32(at.Y));
            this.Deactivate();
            isErasing = false;
            isPenDown = false;
            if (parent.currentDocument.blocks.IndexOf(this) == parent.currentDocument.blocks.Count - 1)
            {
                ParagraphBlock h = new ParagraphBlock();
                parent.currentDocument.blocks.Add(h);
                h.Activate();
           }
            else
            {
                IRenderableBlock b = parent.currentDocument.blocks[parent.currentDocument.blocks.IndexOf(this) + 1];
                b.Activate();
            }
        }
        public string TreeDisplay()
        {
            return "Drawing";
        }

        static int maxPx = 5; // less than 5px is prolly empty
        public bool IsEmpty()
        {
            if (drawing == null) return true;
            if (drawing.Height <= 21) return true;
            int px = 0;
            for (int x = 0; x < drawing.Width; x++)
            {
                for (int y = 0; y < drawing.Height; y++)
                {
                    if (drawing.GetPixel(x, y).R == 0) // count black dots
                        px++;
                    if (px > maxPx) break;
                }
                if (px > maxPx) break;
            }
            return !(px > maxPx);
        }
    }
    
}
