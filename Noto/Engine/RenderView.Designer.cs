﻿namespace Noto
{
    partial class RenderView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // RenderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Name = "RenderView";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.RenderView_Paint);
            this.Click += new System.EventHandler(this.RenderView_Click);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.RenderView_MouseMove);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RenderView_MouseDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RenderView_KeyPress);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RenderView_MouseUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RenderView_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

    }
}
