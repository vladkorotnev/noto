﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.ComponentModel;

namespace Noto
{
    [XmlRoot("Noto")]
    public class NotoDocument
    {
        [XmlArray("Blocks")]
        public List<IRenderableBlock> blocks;
        [XmlIgnore]
        public bool IsSaved = true;
        [XmlIgnore]
        public bool OnDisk = false;
        [XmlIgnore]
        public string DiskPath = "";

        [XmlArray("SHKeys")]
        public List<string> shortcutKeys = new List<string>();
        [XmlArray("SHVals")]
        public List<string> shortcutValues = new List<string>();

        public NotoDocument()
        {
            blocks = new List<IRenderableBlock>();
        }
        public NotoDocument(List<IRenderableBlock> content)
        {
            blocks = new List<IRenderableBlock>();
            foreach (IRenderableBlock r in content)
                if (r != null) blocks.Add(r);
        }
        public void PurgeEmptyBlocks()
        {
            int i = 0;
            while (i < blocks.Count-1)
            {
                IRenderableBlock b = blocks[i];
                if (b.IsEmpty())
                {
                    blocks.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }
        public static Type[] possibleTypes = new Type[]{typeof(MathBlock),typeof(ParagraphBlock), typeof(HeadingBlock), typeof(DrawingBlock)};
        public void WriteToFile(string filePath)
        {
            PurgeEmptyBlocks();
            StringWriter stringWriter = new StringWriter();
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            XmlSerializer x = new XmlSerializer(this.GetType(), possibleTypes);

            writerSettings.OmitXmlDeclaration = true;
            writerSettings.Indent = true;

            using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, writerSettings))
            {
                x.Serialize(xmlWriter, this, ns);
            }
            string dummy = stringWriter.ToString();

            StreamWriter w = File.CreateText(filePath);
            w.Write(dummy);
            w.Close();
            IsSaved = true;
            OnDisk = true;
            DiskPath = filePath;
        }
        public void WriteToFile()
        {
            if (!this.OnDisk) throw new Exception("Tried to write a file without a disk representation present just yet.");
            WriteToFile(this.DiskPath);
        }
        public void WriteToFileInBackground(RunWorkerCompletedEventHandler callback)
        {
            if (!this.OnDisk) throw new Exception("Tried to write a file without a disk representation present just yet.");
            var bg = new BackgroundWorker();
            var myCopy = this.MemberwiseClone();
            bg.DoWork += new DoWorkEventHandler(_ActualBackgroundSave);
            if (callback != null)
            {
                bg.RunWorkerCompleted += callback;
            }
            bg.RunWorkerAsync();
        }

      
        // On worker thread! Only method with real logic
        private void _ActualBackgroundSave(object sender, DoWorkEventArgs e)
        {
            WriteToFile();
        }

        public static NotoDocument DocumentFromFile(string filePath)
        {
            StreamReader f = File.OpenText(filePath);
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            XmlSerializer x = new XmlSerializer((new NotoDocument()).GetType(),possibleTypes);
            NotoDocument d;
            using (XmlReader xmlReader = XmlReader.Create(f))
            {
                d = (NotoDocument) x.Deserialize(xmlReader);
            }
            d.OnDisk = true;
            d.DiskPath = filePath;
            f.Close();
            return d;
        }

    }
}
