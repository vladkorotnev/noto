﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Noto
{
    public partial class RenderView : UserControl
    {
        public RenderView()
        {
            InitializeComponent();
            
            
           
        }
        

        public void LoadSampleDocument()
        {
            currentDocument = new NotoDocument();
            IRenderableBlock h = new HeadingBlock();
            h.HandleTextInputEvent("Noto", this);
            h.Activate();
            h.HandleKeyInputEvent((byte)Keys.I, true, false, false, this);
            currentDocument.blocks.Add(h);
            h = new HeadingBlock(HeadingSizes.ThirdLevel);
            h.HandleTextInputEvent("The best note-taking program of this universe", this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("... or, at least, this sekaisen.", this);
            currentDocument.blocks.Add(h);
            h = new HeadingBlock(HeadingSizes.ThirdLevel);
            h.HandleTextInputEvent("What is Noto", this);
            h.HandleKeyInputEvent((byte)Keys.I, true, false, false, this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("Noto is a note-taking program written by Ak.R. Soft and Genjitsu Gadget Lab in 2015, for Windows Mobile 2003 devices with a hardware keyboard.", this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("Unlike many rich-text-editors, ours doesn't have that much formatting capabilities. Instead it's aimed at blazing fast information recording. " +
                                    "For example, you need to draw a graph from the class' whiteboard. Usually this would mean switching to graphics mode, selecting a tool, inputting vector data, and then reverting back, and that is only if it does not break your formatting."
                                    , this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("In Noto, drawing is as simple as taking your stylus and drawing just like below ", this);
            currentDocument.blocks.Add(h);
            h = new DrawingBlock(Properties.Resources.likethis);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("Noto is intelligent, so you can draw and it extends the space for the picture automatically. ", this);
            h.HandleKeyInputEvent((byte)Keys.I, true, false, false, this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("Try it on the picture above. ", this);
            h.HandleKeyInputEvent((byte)Keys.I, true, false, false, this);
            h.HandleKeyInputEvent((byte)Keys.B, true, false, false, this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("This makes improving your notes with drawings very fast.", this);
            currentDocument.blocks.Add(h);
            if (MimeTexRenderer.canRenderMath)
            {
                h = new ParagraphBlock();
                h.HandleTextInputEvent("Also Noto supports LaTex input.", this);
                h.HandleKeyInputEvent((byte)Keys.I, true, false, false, this);
                h.HandleKeyInputEvent((byte)Keys.B, true, false, false, this);
                currentDocument.blocks.Add(h);
                MathBlock m = new MathBlock();
                m.content = "\\Large\r\n\\int_{-\\infty}^{2\\infty} {\r\n\\vec{\r\n\\frac {\\cyr{Mario}}{SOS(x)}\r\n}\r\ndx\r\n}";
                currentDocument.blocks.Add(m);
                m = new MathBlock();
                m.content = "\\begin{array}{c.c|c} a_1&a_2&a_3 \\\\\\hdash b_1&b_2&b_3 \\\\\\hline c_1&c_2&c_3 \\end{array} ";
                currentDocument.blocks.Add(m);
            }
            h = new HeadingBlock(HeadingSizes.ThirdLevel);
            h.HandleTextInputEvent("Some more capabilities of Noto", this);
            h.HandleKeyInputEvent((byte)Keys.I, true, false, false, this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("- Use CAPS key to toggle Greek layout. Useful if you study a lot of math or physics. Ελληνικα FTW!", this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("- Use Ctrl-1/2/3 to add headings of level 1, 2 and 3 respectively. Simple navigation!", this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("- Ctrl-Arrows and Ctrl-Backspace are supported as well as Ctrl-B (Bold), Ctrl-I (Italic), Ctrl-U (Underline).", this);
            currentDocument.blocks.Add(h);
            h = new HeadingBlock(HeadingSizes.ThirdLevel);
            h.HandleTextInputEvent("About Noto", this);
            h.HandleKeyInputEvent((byte)Keys.I, true, false, false, this);
            h.HandleKeyInputEvent((byte)Keys.U, true, false, false, this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("Noto version 1.0 Alpha by Genjitsu Gadget Lab, 2015", this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("For Windows Mobile 2003 with Hardware Keyboard", this);
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            currentDocument.blocks.Add(h);
            h = new ParagraphBlock();
            h.HandleTextInputEvent("At this place, as usual.", this);
            currentDocument.blocks.Add(h);
            //http://pica-ae.deviantart.com/
            h = new ParagraphBlock();
            h.HandleTextInputEvent("Icon by pica-ae @ deviantart", this);
            currentDocument.blocks.Add(h);
            this.Redraw(true);
        }
        

        private bool didInit = false;
        public void init()
        {
            if (didInit) return;
            didInit = true;
            this.LoadEmptyDocument();
        }

        public void LoadEmptyDocument()
        {
            currentDocument = new NotoDocument();
            HeadingBlock h = new HeadingBlock();
            h.Activate();
            this.currentDocument.blocks.Add(h);
            this.Redraw(true);
        }

        public void LoadDocument(NotoDocument document) {
            currentDocument = document;
            DeactivateAllBlocks();
            currentDocument.blocks[0].Activate();
            this.Redraw(true);
        }

        public NotoDocument currentDocument = new NotoDocument();
        public bool AutosaveEnabled
        {
            get
            {
                return autoSaver != null;
            }
            set
            {
                if (value)
                {
                    autoSaver = new Timer();
                    autoSaver.Interval = 5000;
                    autoSaver.Tick += new EventHandler(autoSaver_Tick);
                    autoSaver.Enabled = true;
                }
                else
                {
                    autoSaver.Dispose();
                    autoSaver = null;
                    _isAutoSaving = false;
                }
            }
        }
        private Timer autoSaver = null;
        private bool _isAutoSaving = false;
        void autoSaver_Tick(object sender, EventArgs e)
        {
            if (_isAutoSaving) return;
            if (currentDocument.OnDisk && !currentDocument.IsSaved)
            {
                try
                {
                    this.SetStateText("Autosaving in background...");
                    _isAutoSaving = true;
                    autoSaver.Enabled = false;
                    this.currentDocument.WriteToFileInBackground(_autosaveDone);
                }
                catch (Exception ex)
                {
                    autoSaver.Dispose();
                    autoSaver = null;
                    _isAutoSaving = false;
                    this.SetStateText("Autosave failed");
                    MessageBox.Show("Autosave failed with error:\r\n" + ex.Message + "\r\nAutosave will be disabled.");
                }
            }
        }

        private void _autosaveDone(object sender, RunWorkerCompletedEventArgs e)
        {
            _isAutoSaving = false;
            this.SetStateText("Autosaved.");
        }


        //public List<IRenderableBlock> currentDocument.blocks = new List<IRenderableBlock>();
        public Dictionary<string, string> shortcuts = new Dictionary<string,string>();
        public float topMargin = 5;
        public float sideMargin = 5;
        Graphics tg;
        public Graphics graphics;
        public void Redraw( bool forceful)
        {
            if(forceful && doubleBuffer != null) {
                doubleBuffer.Dispose();
                doubleBuffer = null;
            }
            this.Invalidate();
        }
        public void Redraw()
        {
            Redraw(false);
        }

        private void RenderView_Click(object sender, EventArgs e)
        {
            
        }
        public VScrollBar scrollbar;
        public bool suppressInput = false;
        private Bitmap doubleBuffer;
        private static Brush wb = new SolidBrush(Color.White);
        private void RenderView_Paint(object sender, PaintEventArgs e)
        {
            Rectangle inside = e.ClipRectangle;
            if (doubleBuffer != null) e.Graphics.DrawImage(doubleBuffer, 0, inside.Y);
            if (doubleBuffer == null)
            {
                doubleBuffer = new Bitmap(this.Width, this.Parent.Height);
                tg = Graphics.FromImage(doubleBuffer);
                graphics = tg;
            }
            tg.FillRectangle(wb, new Rectangle(0, 0, doubleBuffer.Width, doubleBuffer.Height));
            
            Graphics g = Graphics.FromImage(doubleBuffer);//e.Graphics;
            tg = g; graphics = g;
                var i = 0; float origin = topMargin;
                float h = 0;
                while (i < currentDocument.blocks.Count && origin < inside.Y-h)
                {
                    h = currentDocument.blocks[i].RenderingHeight(g, this); 
                  
                    origin += h;
                    i++;
                }
            
               
                if (i < currentDocument.blocks.Count)
                {
                    if (i > 0) { i--; origin -= h; }
                    
                    while (i < currentDocument.blocks.Count && origin < inside.Y + inside.Height )
                    {
                        h =  currentDocument.blocks[i].RenderingHeight(g, this);
                        
                            RectangleF drawRect = new RectangleF(sideMargin, origin - inside.Y, this.Width - 2 * sideMargin, h);
                            g.FillRectangle(wb, 0, Convert.ToInt32(origin), this.Width, Convert.ToInt32(h));
                            currentDocument.blocks[i].RenderOnGraphicsAt(g, drawRect, this);
                        
                        origin += h;
                        i++;
                    }
                }
                RecalculateHeights();
                if (doubleBuffer != null) e.Graphics.DrawImage(doubleBuffer, 0, inside.Y);
        }
        public int paddingBottom = 0;
        void RecalculateHeights()
        {
            if (tg == null) return;
            float height = 0;
            foreach (IRenderableBlock block in currentDocument.blocks)
            {
                height += block.RenderingHeight(tg, this);
            }
            this.Height = Convert.ToInt32(Math.Max(this.Parent.Height-paddingBottom,height+paddingBottom));
        }
        void ScrollToBottom()
        {
            RecalculateHeights();
            Panel p = (Panel)this.Parent;
            p.AutoScrollPosition = new Point(0, (this.Height - (this.Parent.Height - 2*paddingBottom)));
           
        }
       public void ScrollToActive()
        {
            Panel p = (Panel)this.Parent;
            p.AutoScrollPosition = new Point(0, Math.Min((this.Height - (this.Parent.Height - 2 * paddingBottom)), ActiveBlockTop()));
        }
        public void ScrollToBlock(int index,bool makeActive)
        {
            if (index < 0 || index >= currentDocument.blocks.Count) throw new Exception("Out of range for scrolling");
            int h = 0;
            for (int i = 0; i < index; i++)
            {
                h += Convert.ToInt32(currentDocument.blocks[i].RenderingHeight(tg, this));
            }
            Panel p = (Panel)this.Parent;
            p.AutoScrollPosition = new Point(0, Math.Min((this.Height - (this.Parent.Height - 2 * paddingBottom)),h));
            if(makeActive){DeactivateAllBlocks(); currentDocument.blocks[index].Activate();}
        }
        int ActiveBlockTop()
        {
            int h = 0;
            foreach (IRenderableBlock block in currentDocument.blocks)
            {
                if (block.IsActive())
                {
                    return h;
                }
                else
                {
                    h += Convert.ToInt32(block.RenderingHeight(tg, this));
                }
            }
            return h;
        }
        IRenderableBlock ActiveBlock()
        {
            foreach (IRenderableBlock block in currentDocument.blocks)
            {
                if (block.IsActive())
                {
                    return block;
                }
            }
            return null;
        }
        public void GoToPrevBlock()
        {
            int i = ActiveBlockIndex();
            int j = Math.Max(0, i - 1);
            if (j == i) return;
            currentDocument.blocks[i].Deactivate();
            currentDocument.blocks[j].Activate();
        }
        public void GoToNextBlock()
        {
            int i = ActiveBlockIndex();
            int j = Math.Min(currentDocument.blocks.Count-1, i + 1);
            if (j == i) return;
            currentDocument.blocks[i].Deactivate();
            currentDocument.blocks[j].Activate();
        }
        int ActiveBlockIndex()
        {
            return currentDocument.blocks.IndexOf(ActiveBlock());
        }
        public void SetStateText(string to)
        {
            lblState.Text = to;
        }
        private void ResetAutosaveTimer()
        {
            if (AutosaveEnabled && !_isAutoSaving)
            {
                autoSaver.Enabled = false;
                autoSaver.Enabled = true;
            }
        }
        private void RenderView_KeyPress(object sender, KeyPressEventArgs e)
        {
            ResetAutosaveTimer();
            if (suppressInput)
            {
                suppressInput = false; return;
            }
            if (_suppressNextKeyPress) { _suppressNextKeyPress = false; e.Handled = true;  return; }
            IRenderableBlock b = this.ActiveBlock();
            if (b != null && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Enter)
            {
                 b.HandleTextInputEvent(e.KeyChar.ToString(), this);
                 
                 ScrollToActive();
                 this.RedrawActiveBlock();
            }
        }

        static Dictionary<Keys, string> ellinica = new Dictionary<Keys, string> {
           { Keys.Q, ";" } , {Keys.W,"ς"},{Keys.E,"ε"},{Keys.R,"ρ"},{Keys.T,"τ"},{Keys.Y,"υ"},{Keys.U,"θ"},{Keys.I,"ι"},{Keys.O,"ο"},{Keys.P,"π"},
           {Keys.A,"α"},{Keys.S,"σ"},{Keys.D,"δ"},{Keys.F,"φ"},{Keys.G,"γ"},{Keys.H,"η"},{Keys.J,"ξ"},{Keys.K,"κ"},{Keys.L,"λ"},
           {Keys.Z,"ζ"},{Keys.X,"χ"},{Keys.C,"ψ"},{Keys.V,"ω"},{Keys.B,"β"},{Keys.N,"ν"},{Keys.M,"μ"}
        };
        bool ellinicaOn = false;
        public StatusBar lblState;
        private void RenderView_KeyDown(object sender, KeyEventArgs e)
        {
            ResetAutosaveTimer();
            this.currentDocument.IsSaved = false;
            if (e.KeyCode == Keys.Capital)
            {
                // Capslock
                ellinicaOn = !ellinicaOn;
                lblState.Text = "ελληνικά: " + (ellinicaOn ? "ON" : "Off");
                return;
            }
            
            
            IRenderableBlock b = this.ActiveBlock();

            if (b != null)
            {


                if (ellinica.ContainsKey(e.KeyCode) && ellinicaOn)
                {
                    // pass greek alpha

                    var newchar = ellinica[e.KeyCode];
                    if ((e.Modifiers & Keys.Shift) > 0) newchar = newchar.ToUpper();
                    b.HandleTextInputEvent(newchar, this);
                    e.Handled = true;
                    _suppressNextKeyPress = true;
                    this.Redraw();
                    return;
                }
                else if (ellinicaOn)
                {
                    if (e.KeyCode == Keys.ShiftKey)  lblState.Text = "ελληνικά: shift";
                   
                } 
                if ((e.Modifiers != Keys.None && e.Modifiers != Keys.Shift) || e.KeyCode  == Keys.Back || e.KeyCode == Keys.Enter || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
                {
                    b.HandleKeyInputEvent((byte)e.KeyCode, (e.Modifiers & Keys.Control)>0, (e.Modifiers & Keys.Alt)>0, (e.Modifiers & Keys.Shift)>0, this);
                    e.Handled = true;
                }
                
                this.RedrawActiveBlock();
            }
        }
        public void RedrawActiveBlock()
        {
            IRenderableBlock block = currentDocument.blocks[0];
            var i = 0; float origin = 0.0f; float h = 0.0f;
            while (i < currentDocument.blocks.Count && !block.IsActive())
            {
                block = currentDocument.blocks[i];
                h = block.RenderingHeight(tg, this);
                origin += h;
                i++;
            }
            this.Invalidate(new Rectangle(0,Convert.ToInt32(origin-h),this.Width, Convert.ToInt32(h)+150));
        }
        private bool _suppressNextKeyPress = false;
        public void DeactivateAllBlocks()
        {
            foreach (IRenderableBlock block in currentDocument.blocks)
            {
                if (block.IsActive())
                {
                    block.Deactivate();
                }
            }
        }

        float _origin;
        IRenderableBlock landedBlock;
        private void RenderView_MouseDown(object sender, MouseEventArgs e)
        {
            if(AutosaveEnabled)autoSaver.Enabled = false;
            float origin = 0; var i = 0;
            while (i < currentDocument.blocks.Count && origin < e.Y)
            {
                origin += currentDocument.blocks[i].RenderingHeight(tg,this);
                i++;
            }

            if (i < currentDocument.blocks.Count)
            {
                if (i > 0) i--;
                landedBlock = currentDocument.blocks[i];
                origin -= currentDocument.blocks[i].RenderingHeight(tg,this);
            }
            else
            {
                landedBlock = new DrawingBlock();
                lblState.Text = "New drawing";
                currentDocument.blocks.Add(landedBlock);
            }
            this.DeactivateAllBlocks();
            landedBlock.Activate();
            _origin = origin;
            landedBlock.TouchBegan(new Point(Convert.ToInt32(e.X), Convert.ToInt32(e.Y - _origin)), this);
            this.RedrawActiveBlock();
            this.currentDocument.IsSaved = false;
        }
       
        private void RenderView_MouseMove(object sender, MouseEventArgs e)
        {
            if (landedBlock == null) return;
            landedBlock.TouchMoved(new Point(Convert.ToInt32(e.X), Convert.ToInt32( e.Y - _origin)), this);
            this.RedrawActiveBlock();
        }

        private void RenderView_MouseUp(object sender, MouseEventArgs e)
        {
            ResetAutosaveTimer();
            if (landedBlock == null) return;
            landedBlock.TouchEnded(new Point(Convert.ToInt32(e.X), Convert.ToInt32( e.Y - _origin)), this);
            landedBlock = null; this.Redraw();
        }

 

    }
}
