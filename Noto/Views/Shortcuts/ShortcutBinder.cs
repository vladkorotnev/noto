﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Noto
{
    public partial class ShortcutBinder : NonFullscreenForm
    {
        private NotoDocument doc;
        private int index = -1;

        public ShortcutBinder(NotoDocument forDocument)
        {
            InitializeComponent();
            doc = forDocument;
            Size = new Size(this.Width, 145);
        }

        public ShortcutBinder(NotoDocument forDocument, int atIndex)
        {
            InitializeComponent();
            doc = forDocument;
            index = atIndex;
            textBox1.Text = doc.shortcutKeys[index];
            textBox2.Text = doc.shortcutValues[index];
            button1.Text = "Update";
            Size = new Size(this.Width, 145);
            this.Height = 145;
        }
        

        private void ShortcutBinder_Load(object sender, EventArgs e)
        {
           // base.OnLoad(e);
            textBox1.Focus();
            this.FormBorderStyle = FormBorderStyle.None;
          
            Size = new Size(this.Width, 160);
            this.Height = 160;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0 || textBox2.Text.Length == 0) return; 
            if (index == -1)
            {
                doc.shortcutKeys.Add(textBox1.Text);
                doc.shortcutValues.Add(textBox2.Text);
            }
            else
            {
                doc.shortcutKeys[index] = textBox1.Text;
                doc.shortcutValues[index] = textBox2.Text;
            }
        }

        private void ShortcutBinder_GotFocus(object sender, EventArgs e)
        {
            //base.OnGotFocus(e);
            Size = new Size(this.Width, 145);
            this.Height = 145;
        }

        private void ShortcutBinder_Activated(object sender, EventArgs e)
        {
           // base.OnActivated(e);
            Size = new Size(this.Width, 145);
            this.Height = 145;
        }
      
        
    }
}