﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Noto
{
    public partial class ShortcutEditor : Form
    {
        public NotoDocument document;
        public ShortcutEditor(NotoDocument docu)
        {
            InitializeComponent();
            document = docu;
        }

        private void ShortcutEditor_Load(object sender, EventArgs e)
        {
            reloadData();            
        }
        private void reloadData()
        {
            listBox1.Items.Clear(); listBox2.Items.Clear();
            for (int i = 0; i < document.shortcutKeys.Count; i++)
            {
                listBox1.Items.Add(document.shortcutKeys[i]);
                listBox2.Items.Add(document.shortcutValues[i]);
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.SelectedIndex = listBox1.SelectedIndex;
            SelectionChange();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = listBox2.SelectedIndex;
            SelectionChange();
        }
        private void SelectionChange()
        {
            if (listBox1.SelectedIndex >= 0 && listBox1.SelectedIndex < document.shortcutKeys.Count)
            {
                delKey.Enabled = editKey.Enabled = true;
            }
            else
            {
                delKey.Enabled = editKey.Enabled = false;
            }
        }

        private void delKey_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete binding " + listBox1.SelectedItem.ToString() + "=" + listBox2.SelectedItem.ToString() + "?", "Delete binding", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                document.shortcutKeys.RemoveAt(listBox1.SelectedIndex);
                document.shortcutValues.RemoveAt(listBox1.SelectedIndex);
                reloadData();
            }
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            ShortcutBinder b = new ShortcutBinder(document);
            if (b.ShowDialog() != DialogResult.Cancel) reloadData();
        }

        private void editKey_Click(object sender, EventArgs e)
        {
            ShortcutBinder b = new ShortcutBinder(document, listBox1.SelectedIndex);
            if (b.ShowDialog() != DialogResult.Cancel) reloadData();
        }
    }
}