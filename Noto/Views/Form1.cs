﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Noto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MimeTexRenderer.UpdateUsagePath();
            renderView1.init();
            renderView1.lblState = statusBar1;
            renderView1.paddingBottom = statusBar1.Height;
            this.WindowState = FormWindowState.Normal;
            this.WindowState = FormWindowState.Maximized;

            //Utils.ShowMemory();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
           
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

       
        private void menuItem4_Click(object sender, EventArgs e)
        {
            
        }

        private void menuItem10_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to load the About document?", "About Noto", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (!renderView1.currentDocument.IsSaved)
                {
                    DialogResult re = MessageBox.Show("Save file before creating a new document?", "New document", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    switch (re)
                    {
                        case DialogResult.Yes:
                            if (!save()) return;
                            break;
                        case DialogResult.No:
                            break;
                        default:
                            return;
                            break;
                    }
                }
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();
                renderView1.LoadSampleDocument();
                this.Text = "Noto: About Noto";
                statusBar1.Text = "Load: About Noto";
                Cursor.Current = Cursors.Default;
            }
        }

        private void menuItem7_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Greek ςερτυ layout:\r\n\r\n;ςερτυθιοπ\r\nασδφγηξκλ\r\n  ζχψωβνμ\r\n\r\nHit CAPS to toggle Ελληνικα mode.", "About Noto", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void menuItem11_Click(object sender, EventArgs e)
        {
            HeadingBlock h = new HeadingBlock(HeadingSizes.FirstLevel);
            renderView1.currentDocument.blocks.Add(h);
            renderView1.DeactivateAllBlocks();
            h.Activate();
            renderView1.Redraw();
        }

        private void menuItem12_Click(object sender, EventArgs e)
        {
            HeadingBlock h = new HeadingBlock(HeadingSizes.SecondLevel);
            renderView1.currentDocument.blocks.Add(h);
            renderView1.DeactivateAllBlocks();
            h.Activate();
            renderView1.Redraw();
        }

        private void menuItem13_Click(object sender, EventArgs e)
        {
            HeadingBlock h = new HeadingBlock(HeadingSizes.ThirdLevel);
            renderView1.currentDocument.blocks.Add(h);
            renderView1.DeactivateAllBlocks();
            h.Activate();
            renderView1.Redraw();
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            ParagraphBlock h = new ParagraphBlock();
            renderView1.currentDocument.blocks.Add(h);
            renderView1.DeactivateAllBlocks();
            h.Activate();
            renderView1.Redraw();
        }

        private void menuItem8_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Keyboard hotkeys:\r\n" +
                            "* Enter: create new paragraph\r\n"+
            "* Ctrl-1/2/3: create heading\r\n"+
            "* CAPS: toggle Greek\r\n"+
            "* (Ctrl-)TAB: (create)/invoke shortcut\r\n"+
            "* Ctrl-Bksp: erase whole word\r\n"+
            "* Pen: drawing\r\n"+
            "* Pen + Letter: place letter on drawing\r\n"+
            "* Pen + Ctrl: toggle eraser", "About Noto", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void menuItem16_Click(object sender, EventArgs e)
        {
            neu();
        }
        private void neu()
        {
            if (!renderView1.currentDocument.IsSaved)
            {
                DialogResult re = MessageBox.Show("Save file before creating a new document?", "New document", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                switch (re)
                {
                    case DialogResult.Yes:
                        if (!save()) return;
                        break;
                    case DialogResult.No:
                        break;
                    default:
                        return;
                        break;
                }
            }
            renderView1.LoadEmptyDocument();
            this.Text = "Noto: New document";
            statusBar1.Text = "New document";
        }
        private void menuItem25_Click(object sender, EventArgs e)
        {
            if (renderView1.currentDocument.IsSaved)
                Application.Exit();
            else
            {
                DialogResult re = MessageBox.Show("Save file before quitting?", "Quit Noto", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                switch (re)
                {
                    case DialogResult.Yes:
                        if (!save()) return;
                        Application.Exit();
                        break;
                    case DialogResult.No:
                        Application.Exit();
                        break;
                    default:
                        break;
                }
            }
            
        }

        private void menuItem19_Click(object sender, EventArgs e)
        {
            save();
        }

        private bool save()
        {
            try
            {
                if (renderView1.currentDocument.OnDisk)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    Application.DoEvents();
                    renderView1.currentDocument.WriteToFile();
                    Cursor.Current = Cursors.Default;
                    this.Text = "Noto: " + Path.GetFileName(renderView1.currentDocument.DiskPath);
                    statusBar1.Text = "Saved: " + Path.GetFileName(renderView1.currentDocument.DiskPath);
                    renderView1.Redraw();
                    return true;
                }
                else return saveAs();
            }
           catch (Exception e)
            {
                MessageBox.Show("There was an error saving your file: \r\n" + e.Message, "Oh shit", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return false;
            }
        }

        private bool saveAs()
        {
            try {
            SaveFileDialog f = new SaveFileDialog();
            f.Filter = "Noto document|*.noto";
            DialogResult dr;
            dr = f.ShowDialog();
            if (dr != DialogResult.Cancel)
            {
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();
                renderView1.currentDocument.WriteToFile(f.FileName);
                Cursor.Current = Cursors.Default;
                this.Text = "Noto: " + Path.GetFileName(renderView1.currentDocument.DiskPath);
                statusBar1.Text = "Saved: " + Path.GetFileName(renderView1.currentDocument.DiskPath);
                renderView1.Redraw();
            }
            return (dr != DialogResult.Cancel);
            }
            catch (Exception e)
            {
                MessageBox.Show("There was an error saving your file: \r\n" + e.Message, "Oh shit", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return false;
            }
        }

        private void menuItem20_Click(object sender, EventArgs e)
        {
            saveAs();
        }

        private void menuItem18_Click(object sender, EventArgs e)
        {
            open();
        }
        private void open()
        {
            if (!renderView1.currentDocument.IsSaved)
            {
                DialogResult re = MessageBox.Show("Save file before loading another one?", "Open document", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                switch (re)
                {
                    case DialogResult.Yes:
                        if (!save()) return;
                        break;
                    case DialogResult.No:
                        break;
                    default:
                        return;
                        break;
                }
            }
            try
            {
                OpenFileDialog f = new OpenFileDialog();
                f.Filter = "Noto document|*.noto";
                if (f.ShowDialog() != DialogResult.Cancel)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    Application.DoEvents();
                    renderView1.LoadDocument(NotoDocument.DocumentFromFile(f.FileName));
                    this.Text = "Noto: " + Path.GetFileName(renderView1.currentDocument.DiskPath);
                    statusBar1.Text = "Opened: " + Path.GetFileName(renderView1.currentDocument.DiskPath);
                    Cursor.Current = Cursors.Default;
                    
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("There was an error reading your file: \r\n" + e.Message, "Oh shit", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }
        }
        private void menuItem29_Click(object sender, EventArgs e)
        {
            ShortcutEditor s = new ShortcutEditor(renderView1.currentDocument);
            s.Show();
        }


        private void renderView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.S)
            {
                e.Handled = true; renderView1.suppressInput = true;
                save();
            }
      
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.O)
            {
                e.Handled = true; renderView1.suppressInput = true;
                open();
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.N)
            {
                e.Handled = true; renderView1.suppressInput = true;
                neu();
            }
             
        }

        private void showTree_Click(object sender, EventArgs e)
        {
            DocumentTreeView tv = new DocumentTreeView(renderView1.currentDocument);
            if (tv.ShowDialog() == DialogResult.Yes)
            {
                renderView1.ScrollToBlock(tv.SelectedBlockIndex,true);
            }
        }

        private void memMon_Tick(object sender, EventArgs e)
        {
            statusBar1.Text = Utils.ShowMemory();
        }

        private void chkMemMon_Click(object sender, EventArgs e)
        {
            chkMemMon.Checked = !chkMemMon.Checked;
            memMon.Enabled = chkMemMon.Checked;
        }

        private void mnuAutosave_Click(object sender, EventArgs e)
        {
            //mnuAutosave.Checked = !mnuAutosave.Checked;
            renderView1.AutosaveEnabled = !renderView1.AutosaveEnabled;
        }

        private void mnuAutosave_Popup(object sender, EventArgs e)
        {
            
        }

        private void menuItem26_Popup(object sender, EventArgs e)
        {
            // settings
            mnuAutosave.Checked = renderView1.AutosaveEnabled;
        }

        private void menuItem27_Click(object sender, EventArgs e)
        {
            renderView1.currentDocument.PurgeEmptyBlocks();
            renderView1.Redraw();
        }

        private void Form1_Closing(object sender, CancelEventArgs e)
        {
           
        }

        private void menuItem32_Click(object sender, EventArgs e)
        {
            MathBlock b = new MathBlock();
            b.content = "x^2+y^2";
            renderView1.DeactivateAllBlocks();
            b.Activate();
            renderView1.currentDocument.blocks.Add(b);

            renderView1.RedrawActiveBlock();
        }

        private void menuItem33_Click(object sender, EventArgs e)
        {
            MathBlock b = new MathBlock();
            b.content = "";
            renderView1.DeactivateAllBlocks();
            b.Activate();
            renderView1.currentDocument.blocks.Add(b);
            renderView1.RedrawActiveBlock();
            MathInput mi = new MathInput(b);
            mi.Show();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            renderView1.ScrollToActive();
        }

        public void ReturnFromPopout()
        {
            mnuPopout.Checked = false;
            panel1.Parent = this;
            this.Show();
            
            panel1.Dock = DockStyle.Fill;
        }
        public void PerformPopout()
        {
            if (mnuPopout.Checked) return;
            PopoutTypingForm pp = new PopoutTypingForm();
            pp.previousForm = this;
            pp.Text = this.Text;
            panel1.Parent = pp;
            pp.Show();
            this.Hide();
            mnuPopout.Checked = true;
        }

        private void mnuPopout_Click(object sender, EventArgs e)
        {
            PerformPopout();
        }


       

    

       
       
    }
}