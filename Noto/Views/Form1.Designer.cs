﻿namespace Noto
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem16 = new System.Windows.Forms.MenuItem();
            this.menuItem17 = new System.Windows.Forms.MenuItem();
            this.menuItem18 = new System.Windows.Forms.MenuItem();
            this.menuItem19 = new System.Windows.Forms.MenuItem();
            this.menuItem20 = new System.Windows.Forms.MenuItem();
            this.menuItem21 = new System.Windows.Forms.MenuItem();
            this.menuItem22 = new System.Windows.Forms.MenuItem();
            this.menuItem23 = new System.Windows.Forms.MenuItem();
            this.menuItem24 = new System.Windows.Forms.MenuItem();
            this.menuItem25 = new System.Windows.Forms.MenuItem();
            this.menuItem28 = new System.Windows.Forms.MenuItem();
            this.menuItem30 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem11 = new System.Windows.Forms.MenuItem();
            this.menuItem12 = new System.Windows.Forms.MenuItem();
            this.menuItem13 = new System.Windows.Forms.MenuItem();
            this.menuItem33 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem15 = new System.Windows.Forms.MenuItem();
            this.menuItem31 = new System.Windows.Forms.MenuItem();
            this.menuItem29 = new System.Windows.Forms.MenuItem();
            this.showTree = new System.Windows.Forms.MenuItem();
            this.menuItem26 = new System.Windows.Forms.MenuItem();
            this.mnuAutosave = new System.Windows.Forms.MenuItem();
            this.mnuPopout = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItem14 = new System.Windows.Forms.MenuItem();
            this.chkMemMon = new System.Windows.Forms.MenuItem();
            this.menuItem27 = new System.Windows.Forms.MenuItem();
            this.menuItem32 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.statusBar1 = new System.Windows.Forms.StatusBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.renderView1 = new Noto.RenderView();
            this.memMon = new System.Windows.Forms.Timer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem28);
            this.mainMenu1.MenuItems.Add(this.menuItem26);
            this.mainMenu1.MenuItems.Add(this.menuItem5);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.menuItem16);
            this.menuItem1.MenuItems.Add(this.menuItem17);
            this.menuItem1.MenuItems.Add(this.menuItem18);
            this.menuItem1.MenuItems.Add(this.menuItem19);
            this.menuItem1.MenuItems.Add(this.menuItem20);
            this.menuItem1.MenuItems.Add(this.menuItem21);
            this.menuItem1.MenuItems.Add(this.menuItem24);
            this.menuItem1.MenuItems.Add(this.menuItem25);
            this.menuItem1.Text = "File";
            // 
            // menuItem16
            // 
            this.menuItem16.Text = "New";
            this.menuItem16.Click += new System.EventHandler(this.menuItem16_Click);
            // 
            // menuItem17
            // 
            this.menuItem17.Text = "-";
            // 
            // menuItem18
            // 
            this.menuItem18.Text = "Open...";
            this.menuItem18.Click += new System.EventHandler(this.menuItem18_Click);
            // 
            // menuItem19
            // 
            this.menuItem19.Text = "Save";
            this.menuItem19.Click += new System.EventHandler(this.menuItem19_Click);
            // 
            // menuItem20
            // 
            this.menuItem20.Text = "Save as...";
            this.menuItem20.Click += new System.EventHandler(this.menuItem20_Click);
            // 
            // menuItem21
            // 
            this.menuItem21.MenuItems.Add(this.menuItem22);
            this.menuItem21.MenuItems.Add(this.menuItem23);
            this.menuItem21.Text = "Export";
            // 
            // menuItem22
            // 
            this.menuItem22.Text = "HTML Folder";
            // 
            // menuItem23
            // 
            this.menuItem23.Text = "Word document";
            // 
            // menuItem24
            // 
            this.menuItem24.Text = "-";
            // 
            // menuItem25
            // 
            this.menuItem25.Text = "Exit Noto";
            this.menuItem25.Click += new System.EventHandler(this.menuItem25_Click);
            // 
            // menuItem28
            // 
            this.menuItem28.MenuItems.Add(this.menuItem30);
            this.menuItem28.MenuItems.Add(this.menuItem31);
            this.menuItem28.MenuItems.Add(this.menuItem29);
            this.menuItem28.MenuItems.Add(this.showTree);
            this.menuItem28.Text = "Document";
            // 
            // menuItem30
            // 
            this.menuItem30.MenuItems.Add(this.menuItem3);
            this.menuItem30.MenuItems.Add(this.menuItem4);
            this.menuItem30.MenuItems.Add(this.menuItem33);
            this.menuItem30.MenuItems.Add(this.menuItem2);
            this.menuItem30.MenuItems.Add(this.menuItem15);
            this.menuItem30.Text = "Add";
            // 
            // menuItem3
            // 
            this.menuItem3.Text = "Paragraph";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.MenuItems.Add(this.menuItem11);
            this.menuItem4.MenuItems.Add(this.menuItem12);
            this.menuItem4.MenuItems.Add(this.menuItem13);
            this.menuItem4.Text = "Heading";
            // 
            // menuItem11
            // 
            this.menuItem11.Text = "Top level";
            this.menuItem11.Click += new System.EventHandler(this.menuItem11_Click);
            // 
            // menuItem12
            // 
            this.menuItem12.Text = "2 Level";
            this.menuItem12.Click += new System.EventHandler(this.menuItem12_Click);
            // 
            // menuItem13
            // 
            this.menuItem13.Text = "3 Level";
            this.menuItem13.Click += new System.EventHandler(this.menuItem13_Click);
            // 
            // menuItem33
            // 
            this.menuItem33.Text = "Math";
            this.menuItem33.Click += new System.EventHandler(this.menuItem33_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "-";
            // 
            // menuItem15
            // 
            this.menuItem15.Enabled = false;
            this.menuItem15.Text = "You can use the pen to draw";
            // 
            // menuItem31
            // 
            this.menuItem31.Text = "-";
            // 
            // menuItem29
            // 
            this.menuItem29.Text = "Shortcuts";
            this.menuItem29.Click += new System.EventHandler(this.menuItem29_Click);
            // 
            // showTree
            // 
            this.showTree.Text = "Tree";
            this.showTree.Click += new System.EventHandler(this.showTree_Click);
            // 
            // menuItem26
            // 
            this.menuItem26.MenuItems.Add(this.mnuAutosave);
            this.menuItem26.MenuItems.Add(this.mnuPopout);
            this.menuItem26.Text = "Options";
            this.menuItem26.Popup += new System.EventHandler(this.menuItem26_Popup);
            // 
            // mnuAutosave
            // 
            this.mnuAutosave.Text = "Autosave (5s of idle)";
            this.mnuAutosave.Popup += new System.EventHandler(this.mnuAutosave_Popup);
            this.mnuAutosave.Click += new System.EventHandler(this.mnuAutosave_Click);
            // 
            // mnuPopout
            // 
            this.mnuPopout.Text = "Popout";
            this.mnuPopout.Click += new System.EventHandler(this.mnuPopout_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.MenuItems.Add(this.menuItem14);
            this.menuItem5.MenuItems.Add(this.menuItem6);
            this.menuItem5.MenuItems.Add(this.menuItem9);
            this.menuItem5.MenuItems.Add(this.menuItem10);
            this.menuItem5.Text = "Help";
            // 
            // menuItem14
            // 
            this.menuItem14.MenuItems.Add(this.chkMemMon);
            this.menuItem14.MenuItems.Add(this.menuItem27);
            this.menuItem14.MenuItems.Add(this.menuItem32);
            this.menuItem14.Text = "Debug";
            // 
            // chkMemMon
            // 
            this.chkMemMon.Text = "Memory load monitoring";
            this.chkMemMon.Click += new System.EventHandler(this.chkMemMon_Click);
            // 
            // menuItem27
            // 
            this.menuItem27.Text = "Remove empty blocks";
            this.menuItem27.Click += new System.EventHandler(this.menuItem27_Click);
            // 
            // menuItem32
            // 
            this.menuItem32.Text = "Do something";
            this.menuItem32.Click += new System.EventHandler(this.menuItem32_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.MenuItems.Add(this.menuItem7);
            this.menuItem6.MenuItems.Add(this.menuItem8);
            this.menuItem6.Text = "Keys";
            // 
            // menuItem7
            // 
            this.menuItem7.Text = "Greek Layout";
            this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
            // 
            // menuItem8
            // 
            this.menuItem8.Text = "Shortcuts";
            this.menuItem8.Click += new System.EventHandler(this.menuItem8_Click);
            // 
            // menuItem9
            // 
            this.menuItem9.Text = "-";
            // 
            // menuItem10
            // 
            this.menuItem10.Text = "About";
            this.menuItem10.Click += new System.EventHandler(this.menuItem10_Click);
            // 
            // statusBar1
            // 
            this.statusBar1.Location = new System.Drawing.Point(0, 272);
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Size = new System.Drawing.Size(240, 22);
            this.statusBar1.Text = "Welcome to Noto by Genjitsu Gadget Lab";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.renderView1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 272);
            // 
            // renderView1
            // 
            this.renderView1.BackColor = System.Drawing.Color.White;
            this.renderView1.Location = new System.Drawing.Point(0, 0);
            this.renderView1.Name = "renderView1";
            this.renderView1.Size = new System.Drawing.Size(224, 272);
            this.renderView1.TabIndex = 0;
            this.renderView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.renderView1_KeyDown);
            // 
            // memMon
            // 
            this.memMon.Tick += new System.EventHandler(this.memMon_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusBar1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "Noto";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem5;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem menuItem7;
        private System.Windows.Forms.MenuItem menuItem8;
        private System.Windows.Forms.MenuItem menuItem9;
        private System.Windows.Forms.MenuItem menuItem10;
        private System.Windows.Forms.StatusBar statusBar1;
        private System.Windows.Forms.Panel panel1;
        private RenderView renderView1;
        private System.Windows.Forms.MenuItem menuItem16;
        private System.Windows.Forms.MenuItem menuItem17;
        private System.Windows.Forms.MenuItem menuItem18;
        private System.Windows.Forms.MenuItem menuItem19;
        private System.Windows.Forms.MenuItem menuItem20;
        private System.Windows.Forms.MenuItem menuItem21;
        private System.Windows.Forms.MenuItem menuItem22;
        private System.Windows.Forms.MenuItem menuItem23;
        private System.Windows.Forms.MenuItem menuItem24;
        private System.Windows.Forms.MenuItem menuItem25;
        private System.Windows.Forms.MenuItem menuItem26;
        private System.Windows.Forms.MenuItem mnuAutosave;
        private System.Windows.Forms.MenuItem menuItem28;
        private System.Windows.Forms.MenuItem menuItem29;
        private System.Windows.Forms.MenuItem menuItem30;
        private System.Windows.Forms.MenuItem menuItem31;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuItem11;
        private System.Windows.Forms.MenuItem menuItem12;
        private System.Windows.Forms.MenuItem menuItem13;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem15;
        private System.Windows.Forms.MenuItem showTree;
        private System.Windows.Forms.Timer memMon;
        private System.Windows.Forms.MenuItem menuItem14;
        private System.Windows.Forms.MenuItem chkMemMon;
        private System.Windows.Forms.MenuItem menuItem27;
        private System.Windows.Forms.MenuItem menuItem32;
        private System.Windows.Forms.MenuItem menuItem33;
        private System.Windows.Forms.MenuItem mnuPopout;
    }
}

