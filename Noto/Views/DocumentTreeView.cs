﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Noto
{
    public partial class DocumentTreeView : Form
    {
        private NotoDocument doc;
        public int SelectedBlockIndex;
        public DocumentTreeView(NotoDocument ofDocument)
        {
            InitializeComponent();
            doc = ofDocument;
        }

       

        private void DocumentTreeView_Load(object sender, EventArgs e)
        {
            
            TreeNode root = new TreeNode(doc.OnDisk ? Path.GetFileNameWithoutExtension(doc.DiskPath):"Document");
            root.Tag = 0;
            TreeNode currentRoot = root;
            HeadingSizes currentRootHead = HeadingSizes.ThirdLevel;
            
            foreach (IRenderableBlock block in doc.blocks)
            {
                if (block.GetType().Name.ToString() == "HeadingBlock")
                {
                    HeadingBlock h = (HeadingBlock)block;
                    if (h.font.Size < (float)currentRootHead)
                    {
                        // we went deeper
                        TreeNode deeper = new TreeNode(block.TreeDisplay());
                        deeper.Tag = doc.blocks.IndexOf(block);
                        currentRoot.Nodes.Add(deeper);
                        currentRoot = deeper;
                        currentRootHead = (HeadingSizes)h.FontSize;
                    }
                    else
                    {

                        // we went a level up or keeping same level
                        if (currentRoot.Parent != null) currentRoot = currentRoot.Parent;
                        TreeNode higher = new TreeNode(block.TreeDisplay());
                        higher.Tag = doc.blocks.IndexOf(block);
                        currentRoot.Nodes.Add(higher);
                        currentRoot = higher;
                        currentRootHead = (HeadingSizes)h.FontSize;

                    }
                }
                else if (block.GetType().Name.ToString() == "ParagraphBlock")
                {
                    ParagraphBlock p = (ParagraphBlock)block;
                    if (p.FontType != FontStyle.Regular)
                    {
                        // Nongeneric paragraph is likely to be some useful crap
                        TreeNode n = new TreeNode(block.TreeDisplay());
                        n.Tag = doc.blocks.IndexOf(block);
                        currentRoot.Nodes.Add(n);
                    }
                }
                else
                {
                    // most likely a drawing
                    TreeNode n = new TreeNode(block.TreeDisplay());
                    n.Tag = doc.blocks.IndexOf(block);
                    currentRoot.Nodes.Add(n);
                }
            }
            treeView.Nodes.Add(root);
            treeView.Nodes[0].Expand();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            SelectedBlockIndex = (int)treeView.SelectedNode.Tag;
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void contextMenu1_Popup(object sender, EventArgs e)
        {
            if (treeView.SelectedNode != null)
            {
                menuItem1.Enabled = true;
            }
            else
            {
                menuItem1.Enabled = false;
            }
        }
    }
}