﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Noto
{
    public partial class PopoutTypingForm : NonFullscreenForm
    {
        public Form1 previousForm;

        public PopoutTypingForm()
        {
            InitializeComponent();
        }

        private void PopoutTypingForm_Closing(object sender, CancelEventArgs e)
        {
            
            
            previousForm.ReturnFromPopout();
            this.Close();

                
        }

        private void PopoutTypingForm_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            this.Height = Screen.PrimaryScreen.Bounds.Height / 3;
            this.Top = previousForm.Height - this.Height;
        }

        
    }
}